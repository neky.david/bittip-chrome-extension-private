/**
 * Define what css and js should be injecting on what page
 * injection is in background.js in chrome.tabs.onUpdated
 */
var config = {
    "content_scripts": [
        {
            "css": [
                "platforms/youtube/content.css"
            ],
            "js": [
                "platforms/youtube/content.js"
            ],
            "matches": [
                "https://www.youtube.com/watch*"
            ]
        },
        {
            "css": [
                "platforms/stackexchange/content.css"
            ],
            "js": [
                "platforms/stackexchange/content.js"
            ],
            "matches": [
                "https://*.stackexchange.com/questions/*",
                "https://*.stackoverflow.com/questions/*",
                "https://*.serverfault.com/questions/*",
                "https://*.superuser.com/questions/*",
                "https://*.askubuntu.com/questions/*",
                "https://*.stackapps.com/questions/*",
                "https://*.mathoverflow.net/questions/*"
            ]
        },
        {
            "css": [
                "platforms/medium/content.css"
            ],
            "js": [
                "js/jquery-3.6.0.min.js",
                "platforms/medium/content.js"
            ],
            "matches": [
                "https://*.medium.com/*"
            ],
            "favicon_matches": [
                "https://*.medium.com/*"
            ]
        }
    ],
    webUrl: "https://bittip.org/", //"https://bittip.org/" or "http://localhost:8080/",
}