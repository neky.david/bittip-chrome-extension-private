var svg = `
<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" xmlns:bx="https://boxy-svg.com">
  <path d="M 10.949 14.084 C 10.279 15.795 8.34 20.749 7.681 22.437 C 7.637 22.548 7.705 22.669 7.832 22.706 C 8.061 22.773 8.52 22.908 8.75 22.976 C 8.885 23.017 9.035 22.976 9.121 22.877 C 11.118 20.573 16.985 13.799 18.987 11.488 C 19.056 11.408 19.068 11.301 19.018 11.211 C 18.906 11.007 18.65 10.547 18.538 10.345 C 18.486 10.251 18.38 10.193 18.262 10.193 C 17.177 10.193 14.044 10.193 12.942 10.193 C 12.893 10.193 12.858 10.149 12.876 10.108 C 13.634 8.375 15.839 3.334 16.588 1.62 C 16.632 1.521 16.576 1.409 16.462 1.371 C 16.221 1.289 15.638 1.09 15.381 1.002 C 15.308 0.977 15.225 0.999 15.177 1.053 C 13.135 3.38 7.117 10.247 5.081 12.567 C 5.011 12.648 4.985 12.752 5.008 12.85 C 5.064 13.08 5.179 13.554 5.232 13.768 C 5.264 13.902 5.4 13.998 5.556 13.998 C 6.641 13.998 9.759 13.998 10.878 13.998 C 10.929 13.998 10.964 14.042 10.949 14.084 Z" 
  opacity="1" style="stroke-miterlimit: 1; stroke: rgb(0, 0, 0); paint-order: fill; stroke-opacity: 0;" bx:origin="0.490233 0.5"/>
</svg>`;

var site;

function generateTipButtons() {

    //FINDIG SITE from link
    //<link rel="shortcut icon" href="https://cdn.sstatic.net/Sites/skeptics/Img/favicon.ico?v=56b2e5109c57">
    var iconLink = document.querySelector('link[rel="shortcut icon"]');
    var iconLinkUrl = new URL(iconLink.href);
    var pathname = iconLinkUrl.pathname.split('/');
    site = pathname[pathname.indexOf('Sites') + 1];
    //console.log("SITE: - " + site);

    //POSTS
    var votecells = document.querySelectorAll('.votecell .js-voting-container');
    votecells.forEach(votecell => {
        var postId = votecell.attributes['data-post-id'].value;
        var { tipBtn, tooltip } = generatePostTipBtn(postId);
        votecell.appendChild(tipBtn);
        votecell.appendChild(tooltip);
    });

    //COMMNETS
    var comments = document.querySelectorAll('li.comment.js-comment');
    comments.forEach(comment => {
        var commnetId = comment.attributes['data-comment-id'].value;
        var tipBtn = generateCommentTipBtn(commnetId);
        var commnetBody = comment.querySelector('.comment-body')
        commnetBody.appendChild(tipBtn);
    });
}


//Generate new TIP BUTTON in ytButtonPanel
function generatePostTipBtn(postId) {

    //create id
    var id = randomString(8);

    //create BUTTON
    tipBtn = document.createElement('button');
    tipBtn.classList.add('btn-tip', 'grid--cell', 'c-pointer', 'py6', 'mx-auto');
    tipBtn.setAttribute('data-controller', 's-tooltip');
    tipBtn.setAttribute('data-s-tooltip-placement', 'right');
    tipBtn.setAttribute('aria-describedby', '--stacks-s-tooltip-' + id);
    tipBtn.onclick = function () { btnTipClicked(postId, 'post', site) };

    //SVG
    var svgElem = new DOMParser().parseFromString(svg, 'text/html');
    tipBtn.appendChild(svgElem.querySelector('body').firstChild);

    //create TOOLTIP
    var tooltip = document.createElement('div');
    tooltip.id = '--stacks-s-tooltip-' + id;
    tooltip.classList.add("s-popover", "s-popover__tooltip", "pe-none", "wmx2");
    tooltip.setAttribute('aria-hidden', 'true');
    tooltip.setAttribute('role', 'tooltip');
    tooltip.innerText = "Send tip!";

    var arrow = document.createElement('div');
    arrow.classList.add('s-popover--arrow');
    tooltip.appendChild(arrow);

    return { tipBtn, tooltip };
}

//Generate new TIP BUTTON in ytButtonPanel
function generateCommentTipBtn(commentId) {

    //create BUTTON
    tipBtn = document.createElement('div');
    tipBtn.classList.add('btn-tip', 'btn-tip--comment');
    tipBtn.setAttribute('title', 'Send tip!')
    tipBtn.onclick = function () { btnTipClicked(commentId, 'comment', site) };

    //SVG
    var svgElem = new DOMParser().parseFromString(svg, 'text/html');
    tipBtn.appendChild(svgElem.querySelector('body').firstChild);
    // var svgContainer = document.createElement('div');
    // svgContainer.appendChild(svgElem.querySelector('body').firstChild);
    // tipBtn.appendChild(svgContainer);

    return tipBtn;
}

function btnTipClicked(postId, type, site) {
    if (postId && type && site) {
        window.open(
            `${config.webUrl}tip?id=${postId}&platform=stackexchange&type=${type}&site=${site}`,
            'winname',
            'directories=no,titlebar=yes,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=400,height=680'
        );
    }
    else
        console.log('postId, type or site is missing');
}

console.log("BitTip - StackExchange content script running!");
generateTipButtons();