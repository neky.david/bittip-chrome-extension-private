var svg = `
<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <path d="M 10.454 14.092 C 9.784 15.803 7.845 20.757 7.186 22.445 C 7.142 22.556 7.21 22.677 7.337 22.714 C 7.566 22.781 8.025 22.916 8.255 22.984 C 8.39 23.025 8.54 22.984 8.626 22.885 C 10.623 20.581 16.49 13.807 18.492 11.496 C 18.561 11.416 18.573 11.309 18.523 11.219 C 18.411 11.015 18.155 10.555 18.043 10.353 C 17.991 10.259 17.885 10.201 17.767 10.201 C 16.682 10.201 13.549 10.201 12.447 10.201 C 12.398 10.201 12.363 10.157 12.381 10.116 C 13.139 8.383 15.344 3.342 16.093 1.628 C 16.137 1.529 16.081 1.417 15.967 1.379 C 15.726 1.297 15.143 1.098 14.886 1.01 C 14.813 0.985 14.73 1.007 14.682 1.061 C 12.64 3.388 6.622 10.255 4.586 12.575 C 4.516 12.656 4.49 12.76 4.513 12.858 C 4.569 13.088 4.684 13.562 4.737 13.776 C 4.769 13.91 4.905 14.006 5.061 14.006 C 6.146 14.006 9.264 14.006 10.383 14.006 C 10.434 14.006 10.469 14.05 10.454 14.092 Z" 
  opacity="1" fill-opacity="0"/>
</svg>`;

function generateTipButtons(delayed = false) {
    // console.log(`bittip generated for URL - ${window.bittipGeneratedFor}`);
    // console.log(`             current URL - ${window.location.href}`);
    
    var tipBtns = document.getElementsByClassName('btn-tip')

    if (window.bittipGeneratedFor != window.location.href || delayed && tipBtns.length === 0) {
        console.log('GENERATING BIT TIP BUTTON!');
        window.bittipGeneratedFor = window.location.href;

        const responses = document.querySelectorAll('button[aria-label="responses"]')
        if (responses) {
            responses.forEach((response, key) => {
                response = $(response)
                if (key === 0) {
                    const parents = $(response.parents()[2])
                    const delimiter = parents.children()[1].cloneNode(true)
                    appendTipButton(parents, delimiter)
                } else if (key === 1) {
                    const parents = $(response.parents()[3])
                    const delimiter = document.createElement('div')
                    delimiter.classList.add('left-btn-delimiter')
                    appendTipButton(parents, delimiter)
                } else {
                    throw Error
                }
            });
        }
    }
}

function appendTipButton(parents, delimiter = null) {
    if (parents.length === 1 && !$(parents[0]).find('.btn-tip').length) {
        var tipBtn = generateTipBtn(window.bittipGeneratedFor)
        if (delimiter) parents[0].appendChild(delimiter)
        parents[0].appendChild(tipBtn)
    }
}

//Generate new TIP BUTTON in ytButtonPanel
function generateTipBtn(mediumUrl) {

    //create BUTTON
    tipBtn = document.createElement('button');
    tipBtn.classList.add('btn-tip');
    tipBtn.onclick = function () { btnTipClicked(mediumUrl) };
    //on hover on btn set tooltip position (bottom or top)
    tipBtn.addEventListener("mouseover", positionTooltip);
    //reset to DEFAULT
    tipBtn.addEventListener("mouseout", resetTooltipPosition);

    //IMG
    var svgElem = $(svg)[0];
    tipBtn.appendChild(svgElem);

    //create TOOLTIP
    var tooltip = document.createElement('span');
    tooltip.innerText = "Send tip!";
    tooltip.classList.add('tooltip'); //, 'tooltip--bottom');
    tipBtn.appendChild(tooltip);

    return tipBtn;
}

//set position of tooltip acording to position (bottom or top)
function positionTooltip() {
    var tooltip = this.parentNode.querySelector(".tooltip");

    // Tooltip coordinates and size
    var tooltip_rect = tooltip.getBoundingClientRect();

    console.log(tooltip_rect);
    console.log('(tooltip_rect.y + tooltip_rect.height) > 0');
    console.log(`${tooltip_rect.y} > 0 - ${(tooltip_rect.y > 0)}`);

    // Corrections if out of window
    if (tooltip_rect.y < 0)
        tooltip.classList.add("tooltip--bottom");
}

//remove tooltip-top from tooltip
function resetTooltipPosition() {
    var tooltip = this.parentNode.querySelector(".tooltip");
    tooltip.classList.remove("tooltip--bottom");
}

function btnTipClicked(mediumUrl) {
    if (mediumUrl) {
        window.open(
            `${config.webUrl}tip?id=${mediumUrl}&platform=medium`,
            'winname',
            'directories=no,titlebar=yes,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=400,height=680'
        );
    }
    else
        console.log('medium URL is missing');
}

console.log("BitTip - Medium content script running!")
generateTipButtons()

var tipBtns = document.getElementsByClassName('btn-tip')
if (tipBtns.length === 0) {
    setTimeout(generateTipButtons, 2000, true)
}