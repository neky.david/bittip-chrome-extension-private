var svg = `
<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <path d="M 10.454 14.092 C 9.784 15.803 7.845 20.757 7.186 22.445 C 7.142 22.556 7.21 22.677 7.337 22.714 C 7.566 22.781 8.025 22.916 8.255 22.984 C 8.39 23.025 8.54 22.984 8.626 22.885 C 10.623 20.581 16.49 13.807 18.492 11.496 C 18.561 11.416 18.573 11.309 18.523 11.219 C 18.411 11.015 18.155 10.555 18.043 10.353 C 17.991 10.259 17.885 10.201 17.767 10.201 C 16.682 10.201 13.549 10.201 12.447 10.201 C 12.398 10.201 12.363 10.157 12.381 10.116 C 13.139 8.383 15.344 3.342 16.093 1.628 C 16.137 1.529 16.081 1.417 15.967 1.379 C 15.726 1.297 15.143 1.098 14.886 1.01 C 14.813 0.985 14.73 1.007 14.682 1.061 C 12.64 3.388 6.622 10.255 4.586 12.575 C 4.516 12.656 4.49 12.76 4.513 12.858 C 4.569 13.088 4.684 13.562 4.737 13.776 C 4.769 13.91 4.905 14.006 5.061 14.006 C 6.146 14.006 9.264 14.006 10.383 14.006 C 10.434 14.006 10.469 14.05 10.454 14.092 Z" 
  opacity="1" fill-opacity="0"/>
</svg>`;

var ytButtonPanel;
var tipBtn;
var titleElem;
var isAdding;


/**
 * Fire when page was changed (other video)
 */
function pageChanged() {
  var newUrl = new URL(window.location);

  //new url is video
  if (newUrl.hostname == 'www.youtube.com' && newUrl.pathname == '/watch') {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
    console.log("VideoId: " + params.v);
    addTipButton(params.v);
  }
}

async function addTipButton(videoId) {
  if (!tipBtn && !isAdding) {
    isAdding = true;
    if (!ytButtonPanel) {
      findYTButtonPanel();
      while (!ytButtonPanel) {
        await timeout(300);
        findYTButtonPanel();
      }
    }

    if (ytButtonPanel) {
      createTipBtn(videoId);
      isAdding = false;
      return;
    }
  }
}

//Find out right YouTube button panel and save it to ytButtonPanel variable
function findYTButtonPanel() {
  //find YouTube Button Panel
  var panel = document.querySelector('#content #primary #info #menu-container #menu #top-level-buttons-computed');

  if (panel && panel.children.length > 0) {
    ytButtonPanel = panel;

    observeButtonPanel(ytButtonPanel);
  }
}

//Generate new TIP BUTTON in ytButtonPanel
function createTipBtn(videoId) {
  //console.log('creating tip button');
  if (ytButtonPanel) {
    //console.log('creating...');
    //create BUTTON
    tipBtn = document.createElement('div');
    tipBtn.classList.add('btn-tip');
    tipBtn.onclick = function () { btnTipClicked(videoId) };
    //on hover on btn set tooltip position (bottom or top)
    tipBtn.addEventListener("mouseover", positionTooltip);
    //reset to DEFAULT
    tipBtn.addEventListener("mouseout", resetTooltipPosition);

    //SVG
    var svgElem = new DOMParser().parseFromString(svg, 'text/html');
    tipBtn.appendChild(svgElem.querySelector('body').firstChild);

    //create TOOLTIP
    var tooltip = document.createElement('span');
    tooltip.innerText = "Send tip!";
    tooltip.classList.add('tooltip');
    tipBtn.appendChild(tooltip);

    //INSERT button
    if (ytButtonPanel.children.length > 0) {
      //there is more than 2 btns.. add BUTTON to 3rd place
      if (ytButtonPanel.children.length > 2)
        ytButtonPanel.insertBefore(tipBtn, ytButtonPanel.children[2]);
      //otherwise add BUTTON to 1st place
      else
        ytButtonPanel.insertBefore(tipBtn, ytButtonPanel.firstChild);
    }
  }
}

function btnTipClicked(videoId) {
  window.open(
    `${config.webUrl}tip?id=${videoId}&platform=youtube`,
    'winname',
    'directories=no,titlebar=yes,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=400,height=680'
  );
}

//Observe YouTube button panel to prevent TIP button to delete
//docs: https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
//docs: https://dom.spec.whatwg.org/#mutation-observers
function observeButtonPanel() {

  // Callback function to execute when mutations are observed
  const callback = function (mutationsList, observer) {
    for (const mutation of mutationsList) {
      if (mutation.type === 'childList'
        && mutation.removedNodes.length > 0
        && mutation.removedNodes[0].classList.contains('btn-tip')) {
        console.log("BTN-TIP WAS DELETED");
        ytButtonPanel.insertBefore(mutation.removedNodes[0], mutation.nextSibling);
      }
    }
  };

  const observer = new MutationObserver(callback);
  // Start observing
  observer.observe(ytButtonPanel, { childList: true });
}

//set position of tooltip acording to position (bottom or top)
function positionTooltip() {
  var tooltip = this.parentNode.querySelector(".tooltip");

  // Tooltip coordinates and size
  var tooltip_rect = tooltip.getBoundingClientRect();

  // Corrections if out of window
  if ((tooltip_rect.y + tooltip_rect.height) > window.innerHeight)
    tooltip.classList.add("tooltip-top");
}

//remove tooltip-top from tooltip
function resetTooltipPosition() {
  var tooltip = this.parentNode.querySelector(".tooltip");
  tooltip.classList.remove("tooltip-top");
}

// //RECEIVE MESSAGE FORM BACKGROUND.JS
// chrome.runtime.onMessage.addListener(
//   function (request, sender, sendResponse) {
//     if (request.message == "NEW_VIDEO_URL" && request.newUrl) {
//       var url = new URL(request.newUrl);
//       console.log(url);
//       var videoId = url.searchParams.get("docid");
//       console.log(videoId);

//       if (!tipBtn)
//         addTipButton();
//     }
//     else {
//       console.log(request);
//     }
//   });


console.log("BitTip - YouTube content script running!");
pageChanged();