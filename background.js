importScripts('config.js');
importScripts('js/url-match-patterns.js');

//on TAB UPDATE
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    // read changeInfo data and do something with it
    // like send the new url to contentscripts.js

    // console.log(changeInfo);
    // console.log(tab);

    if (changeInfo.status == 'loading') {
        injectContent(tab.id, tab.url, null);
    } else if (changeInfo.favIconUrl) {
        injectContent(tab.id, null, changeInfo.favIconUrl);
    }
});

function injectContent(tabId, url, faviconUrl) {

    console.log(`injectContent(${tabId}, ${url}, ${faviconUrl})`);

    var matching = false;

    if (url) {
        for (var i = 0; i < config.content_scripts.length; i++) {
            var cfg = config.content_scripts[i];
            matching = urlIsMatchingConfig(url, cfg);

            // console.log(cfg);
            // console.log(`MATCHING - ${matching}`);

            if (matching) {
                tryInjectFilesToTab(tabId, cfg);
                i = config.content_scripts.length;
            }
        }
    }

    if (!matching && faviconUrl) {
        for (var i = 0; i < config.content_scripts.length; i++) {
            var cfg = config.content_scripts[i];
            matching = cfg.favicon_matches && faviconIsMatchingConfig(faviconUrl, cfg);

            // console.log(cfg);
            // console.log(`MATCHING - ${matching}`);

            if (matching) {
                tryInjectFilesToTab(tabId, cfg);
                i = config.content_scripts.length;
            }
        }
    }
}

function urlIsMatchingConfig(url, cfg) {
    var matching = false;

    if (url && cfg.matches)
        cfg.matches.every(pattern => { //every() is like foreach() but stops when return false;
            matching = match(pattern, url);
            console.log(`${url} matching ${pattern}? - ${matching}`);
            return !matching;
        });

    return matching;
}

function faviconIsMatchingConfig(faviconUrl, cfg) {
    var matching = false;

    if (faviconUrl && cfg.favicon_matches)
        cfg.favicon_matches.every(pattern => {
            matching = match(pattern, faviconUrl);
            console.log(`${faviconUrl} matching ${pattern}? - ${matching}`);
            return !matching;
        })

    return matching;
}


function tryInjectFilesToTab(tabId, cfg) {

    cfg.css.forEach(function (css) {
        //console.log(`CSS INJECT: ${css}`);
        chrome.scripting.insertCSS({
            target: { tabId },
            files: [css],
        });
    })

    chrome.scripting.executeScript({
        target: { tabId },
        files: ["config.js"]
    });

    chrome.scripting.executeScript({
        target: { tabId },
        files: ["js/utility.js"]
    });

    cfg.js.forEach(function (js) {
        //console.log(`JS INJECT: ${js}`);
        chrome.scripting.executeScript({
            target: { tabId },
            files: [js]
        });
    })

}



//ON INSTALL
// chrome.runtime.onInstalled.addListener((reason) => {
//   if (reason === chrome.runtime.OnInstalledReason.INSTALL) {
//     chrome.tabs.create({
//       url: 'onboarding.html'
//     });
//   }
// });


//SEND MESAGE TO CONTENT.JS
// chrome.tabs.sendMessage(tabId, {
//   message: 'hello!',
//   tab: tab,
//   url: changeInfo.url
// })